//Inicio de programa

//Parametros

float angulo;

//Programa
void setup(){
  size(600,600);
  smooth();
  angulo=0;
}

void draw(){
  rectangulos();
  fill(0,5);
  rect(0,0,width,height);

  fill(255);
  background(0);
  stroke(100);
  ellipse(300,300,5,5);
  
  angulo = angulo+0.02;
  translate(300,300);
  rotate(angulo);
  rect(0,0,5,150);
  
}

 void rectangulos(){
   
  background(255);
  noStroke();
  fill(255);
  rect(20,20,50,20);
 }
